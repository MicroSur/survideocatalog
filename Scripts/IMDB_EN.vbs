'
'us.IMDB.com html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2007-2008.
'no picture, try SCVAmazonCovers.exe
'

Option Explicit
Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://us.imdb.com/find?tt=1;q="

Dim BaseAddress
BaseAddress = "http://us.imdb.com/title/"

Dim NoCastFlag
'NoCastFlag = False 'Johnny Depp ... Jack Sparrow
NoCastFlag = True  'Johnny Depp


Sub AnalyzePage()
 Dim LineNr, i, j
 Dim Line, StartPos, EndPos
 Dim ItemID, MovieAddress, MovieTitle
 Dim Value

LineNr = SVC.FindLine("<title>IMDb", 0)
If LineNr > -1 Then '������ ���������
 LineNr = SVC.FindLine(">No Matches", 0)
 If LineNr > -1 Then 
  MTitles(0) = "IMDb: No Matches."
  Exit Sub '������ ���
 End If

' LineNr = SVC.FindLine(">Popular Titles", 0)
' If LineNr > -1 Then 
  AddMoviesTitles (LineNr)
' Else
'  LineNr = SVC.FindLine("Partial Matches", 0)
'  If LineNr > -1 Then 
'   AddMoviesTitles (LineNr)
'  Else
'   LineNr = SVC.FindLine("Approx Matches", 0)
'   If LineNr > -1 Then AddMoviesTitles (LineNr)
'  End If
' End If

Else '������������� ���� ��������� ��������
    AnalyzeMoviePage 0
    SVC.PutToSVC
    Exit Sub
End If

End Sub


Sub AddMoviesTitles(LineNr)
  Dim Line, MovieTitle, MovieAddress, Value, Block
  Dim StartPos, EndPos, i, n, StartLineNr

StartLineNr = LineNr
i = 0

'����������
LineNr = SVC.FindLine(">Popular Titles", StartLineNr)
If LineNr > -1 Then 
 Block = SVC.GetBlockFrom(">Popular Titles", "</table>")
 n = SVC.MyVal(Block)
 If n > 0  Then
  FillItems n, i,  Block, "--- Popular Titles (" & n & "):"
 End If
End If
 
'������ ����������
LineNr = SVC.FindLine(">Titles (Exact", StartLineNr)
If LineNr > -1 Then 
 Block = SVC.GetBlockFrom(">Titles (Exact", "</table>")
 n = SVC.MyVal(Block)
 If n > 0  Then 
  FillItems n, i,  Block, "--- Exact matches (" & n & "):"
 End If
End If

'��������� ���������
LineNr = SVC.FindLine(">Titles (Partial", StartLineNr)
If LineNr > -1 Then 
 Block = SVC.GetBlockFrom(">Titles (Partial", "</table>")
 n = SVC.MyVal(Block)
 If n > 0  Then 
  FillItems n, i, Block, "--- Partial matches (" & n & "):"
 End If
End If

'��������������
LineNr = SVC.FindLine(">Titles (Approx", StartLineNr)
If LineNr > -1 Then 
 Block = SVC.GetBlockFrom(">Titles (Approx", "</table>")
 n = SVC.MyVal(Block)
 If n > 0  Then 
  FillItems n, i, Block, "--- Other matches (" & n & "):"
 End If
End If
End Sub

Sub FillItems (n, i, Block, sMatchesTitle)
  Dim MovieTitle, MovieAddress, Value
  Dim StartPos, EndPos, StartLineNr

If UBound(MTitles) <> 0 Then Redim Preserve MTitles(UBound(MTitles) + 1)
MTitles(UBound(MTitles)) = sMatchesTitle
i = i + 1
StartPos = 1
For i = i to i + n - 1
 If StartPos > 0 Then
  StartPos = InStr(StartPos, Block, "<a href=""/title/")
  If StartPos > 0 Then
   StartPos = StartPos + 16
   EndPos = InStr(StartPos, Block, "/""") + 1 
   If EndPos > StartPos Then
    MovieAddress = Mid(Block, StartPos, EndPos - StartPos)
    StartPos = StartPos - 16
    EndPos = InStr(StartPos, Block, ")<") + 2 '� �����
    'EndPos = InStr(StartPos, Block, "</a")
    If EndPos > StartPos Then
     Value = Mid(Block, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(Value)
     StartPos = EndPos
    End If
    ReDim Preserve MTitles(i): MTitles(i) = MovieTitle
    ReDim Preserve MTitlesURL(i)
    MTitlesURL(i) = BaseAddress & MovieAddress
   End If
  End If
 End If
Next
End Sub


Sub AnalyzeMoviePage(Ind)
 Dim Line, Value, Block, Temp
 Dim LineNr, m
 Dim BeginPos, EndPos

'��������
 Block = SVC.GetBlockFrom("<h1><strong class=""title"">", "</strong>")
 If Len(Block) = 0 Then
  Block = SVC.GetBlockFrom("<title>", "</title>")
 End If
 BeginPos = 1
 'EndPos = InStr(Block, "(<")
 EndPos = InStr(Block, "(")
 If EndPos > BeginPos Then
  Value = Mid(Block, BeginPos, EndPos - BeginPos)
  MTitle = SVC.HTML2TEXT(Value)
 End If

'���
 BeginPos = EndPos
 EndPos = Len(Block)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Block, BeginPos, EndPos - BeginPos)
  Value = SVC.MyVal(Value)
  MYear = SVC.HTML2TEXT(Value)
 End If

'���
 LineNr = SVC.FindLine(">Direct", 0)
 If LineNr > -1 Then 
  LineNr = LineNr + 1
  Line = SVC.PageArr(LineNr)
  Value = Replace(Line, "<br>", ", ")
  Value = Replace(Value, CHR(13), "")
  Value = Replace(Value, "more", "")
  If Right(Value, 2) = ", " Then Value = Left(Value, Len(Value) - 2)
  MDirector = SVC.HTML2TEXT(Value)
 End If

'����
 LineNr = SVC.FindLine(">Genre:", LineNr)
 If LineNr > -1 Then 
  LineNr = LineNr + 1
  Line = SVC.PageArr(LineNr)
  Value = Replace(Line, " / ", ", ")   
  Value = Replace(Line, " | ", ", ")   
  Value = Replace(Value, "more", "")
  MGenre = SVC.HTML2TEXT(Value)
 End If

'��������
 Value = ""
 LineNr = SVC.FindLine("<h5>Tagline:</h5>", 1)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  Line = Line & SVC.PageArr(LineNr + 1)
  Value = Replace(Line, "Plot Outline:", "")
  Value = Replace(Value, "more", "")
  Value = Replace(Value, "(view trailer)", "")
  Temp =  SVC.HTML2TEXT(Value)
  Value = ""
 End If
 '�
 LineNr = SVC.FindLine("<h5>Plot:</h5>", 1)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  Line = Line & SVC.PageArr(LineNr + 1)
  Value = Replace(Line, "Plot Summary:", "")
  Value = Replace(Value, "more", "")
  Value = Replace(Value, "(view trailer)", "")
 End If

 Value = SVC.HTML2TEXT(Value)
 Value = Replace(Value, "full summary | full synopsis", "")

 If (Len(Temp) > 0) And (Len(Value) > 0) Then
  MDescription = Temp & vbCrLf & Value
 Else 
  MDescription = Temp & Value
 End If

'�������
 Block = SVC.GetBlockFrom("<div class=""meta"">", "/")
 If Instr(Block,"awaiting") = 0 Then 
  MRating = SVC.MyVal(Block)
 End If

'������
 LineNr = SVC.FindLine("Cast overview", LineNr)
 If LineNr < 0 Then LineNr = SVC.FindLine("Series Cast", LineNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  Value = Replace(Line, "Cast overview, first billed only:", "")
  Value = Replace(Line, "(Cast overview, first billed only)", "")
  Value = Replace(Value, "Cast overview:", "")
  Value = Replace(Value, "(Cast overview)", "")
  Value = Replace(Value, "Series Cast Summary:", "")
  Value = Replace(Value, "(Series Cast Summary)", "")
  Value = Replace(Value, "Cast", "")
  Value = Replace(Value, "</tr>", ",")

  If NoCastFlag Then
   Temp = ""
   Do While Value <> Temp
    Temp = Value
    Value = SVC.DelBlock(Value,"...","</td>,")
   Loop
   Value = Replace(Value, " ,", ",")
  End If

  Value = SVC.HTML2TEXT(Value)

  m = InStr(Value, "more")
  If m > 1 Then  Value = Left(Value, m - 1)
'  Value = Replace(Value, "(more)", "")
'  Value = Replace(Value, "more", "")

  MActors = Value
 End If

' '���
' If Len(Value) = 0 Then
'  LineNr = SVC.FindLine("Credited cast", LineNr)
'  If LineNr > -1 Then
'   Line = SVC.PageArr(LineNr)
'   Value = Replace(Line, "Credited cast:", "")
'   Value = Replace(Line, "(Credited cast)", "")
'   Value = Replace(Value, "(more)", "")
'   Value = Replace(Value, "more", "")
'   MActors = SVC.HTML2TEXT(Value)
'  End If
' End If

'������
 Block = SVC.GetBlockFrom("<h5>Country:</h5>", "</div>")
 If Len(Block) <> 0 Then
  Value = SVC.HTML2TEXT(Block)
  Value = Replace(Value, "Country: ", "")
  Value = Replace(Value, " / ", ", ")
  MCountry = Replace(Value, " | ", ", ")
 
 End If

'��������
'������ ��� � ����������. ����������� SCVAmazonCovers.exe

End Sub





