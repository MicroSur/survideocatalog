'
'DVD-Film-Shop.ru html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2005-2007.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://dvd-film-shop.ru/cgi-bin/search.cgi?text="
Dim BaseAddress
BaseAddress = "http://dvd-film-shop.ru"

Public Sub AnalyzePage()
 Dim LineNr
 Dim Line

LineNr = SVC.FindLine("<p>��������� ������:<ul></ul></p><p><font color=#FF0000 size=+1><b>� ���������, ������ �� �������!", 0)
If LineNr < 0 Then
 AddMoviesTitles
Else
 MTitles(0) = "DVDFilmShop: ������ �� �������."   
End If
End Sub

Private Sub AddMoviesTitles()
 Dim Line, MovieTitle, MovieAddress
 Dim BeginPos, EndPos, EndTitlePos, LineNr, i

LineNr = SVC.FindLine("<p>��������� ������:<ul>", 0)
If LineNr > -1 Then

 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "<li><a href='") + 13
 
 i = 0 
 Do While BeginPos > 13
  EndPos = InStr(BeginPos, Line, "'>")
  If EndPos > BeginPos And BeginPos > 0 Then
   MovieAddress = Mid(Line, BeginPos, EndPos - BeginPos)
   Line = Right(Line, Len(Line) - EndPos -1)
   BeginPos = 1
   EndPos = InStr(BeginPos, Line, "</li>")
   If EndPos > BeginPos And BeginPos > 0 Then
    MovieTitle = Mid(Line, BeginPos, EndPos - BeginPos)
    EndTitlePos = InStr(MovieTitle, "</a>")
    ReDim Preserve MData(i): MData(i) = SVC.HTML2TEXT(Mid(MovieTitle, BeginPos, EndTitlePos - BeginPos))
    MovieTitle = SVC.HTML2TEXT(MovieTitle)
    Redim Preserve MTitles(i): MTitles(i) = MovieTitle
    ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
    i = i + 1
   End If
  End If
  Line = Right(Line, Len(Line) - EndPos + 1)
  BeginPos = InStr(Line, "<li><a href='") + 13
 Loop

End If
End Sub



Public Sub AnalyzeMoviePage(Ind)
 Dim Line, Value 
 Dim BeginPos, EndPos, LineNr, i

'Title
MTitle = MData(Ind)
  
'Description
LineNr = SVC.FindLine("������� �", 0)
If LineNr > 0 Then
 LineNr = LineNr + 1
 Line = SVC.PageArr(LineNr)
 MDescription = SVC.HTML2TEXT(Line)
End If
  
'����
LineNr = SVC.FindLine("<b>����:</b>", 0)
If LineNr > 0 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "����:") + 5
 EndPos = Len(Line)
 Value = Mid(Line, BeginPos, EndPos - BeginPos)
 Value = Replace(Value,";",",")
 MGenre = SVC.HTML2TEXT(Value)
End If

'����
'LineNr = SVC.FindLine("<b>�������� �������:</b>", 0)
'If LineNr > 0 Then
' Line = SVC.PageArr(LineNr)
' BeginPos = InStr(Line, "�������:") + 8
' EndPos = Len(Line)
' Value = Mid(Line, BeginPos, EndPos - BeginPos)
' Value = Replace(Value,";",",")
' MLang = SVC.HTML2TEXT(Value)
'End If

'��������
'LineNr = SVC.FindLine("<b>��������:</b>", 0)
'If LineNr > 0 Then
' Line = SVC.PageArr(LineNr)
' BeginPos = InStr(Line, "��������:") + 9
' EndPos = Len(Line)
' Value = Mid(Line, BeginPos, EndPos - BeginPos)
' Value = Replace(Value,";",",")
' MSubt = SVC.HTML2TEXT(Value)
'End If

'Country, Length and Year
LineNr = SVC.FindLine(" �,", LineNr)
If LineNr > 0 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = 1
 EndPos = InStr(Line, " �,")
 If EndPos > BeginPos And BeginPos > 0 Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MYear = SVC.HTML2TEXT(Value)
 End If
End If

BeginPos = InStr(Line, "<a href")
EndPos = Len(Line)
If EndPos > BeginPos And BeginPos > 0 Then
 Value = Mid(Line, BeginPos, EndPos - BeginPos)
 Value = Replace(Value,";",",")
 MCountry = SVC.HTML2TEXT(Value)
End If

'Director
LineNr = SVC.FindLine(">��������", 0) + 1'3
If LineNr > 2 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "title='") + 7
 Value = vbNullString
 Do While BeginPos > 7
  EndPos = InStr(BeginPos, Line, "'>")
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Value + Mid(Line, BeginPos, EndPos - BeginPos) & ", "
  End If
  LineNr = LineNr + 3
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "title='") + 7
 Loop
 If Len(Value) > 2 Then
  MDirector = Left(Value, Len(Value) - 2) 
 End If
End If
  
'Actors
LineNr = SVC.FindLine("� �����", 0) + 3
Value = vbNullString
If LineNr > 2 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "title='") + 7
 Do While BeginPos > 7
  EndPos = InStr(BeginPos, Line, "'>")
  If EndPos > BeginPos Then
   Value = Value + Mid(Line, BeginPos, EndPos - BeginPos) & ", "
  End If
  LineNr = LineNr + 1
  Line = SVC.PageArr(LineNr)
  If Instr(Line, "������ ������") > 0 Then Exit Do
  LineNr = LineNr + 1
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "title='") + 7
 Loop
 If Len(Value) > 3 Then MActors = Left(Value, Len(Value) - 2)
End If

'Picture
LineNr = SVC.FindLine("cover border=0 src=", 0)
If LineNr > 0 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "http://")
 EndPos = InStr(BeginPos, Line, "jpg'") + 3
 If EndPos > BeginPos And BeginPos > 0 Then
  MPicURL = Mid(Line, BeginPos, EndPos - BeginPos)
 End If
End If

End Sub
