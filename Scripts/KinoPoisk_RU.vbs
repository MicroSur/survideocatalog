'
'kinopoisk.ru html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2007.
'
Option Explicit
Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Dim sPoster(), tPoster(), idPoster()
Redim MTitles(0)
Dim url
url = "http://www.kinopoisk.ru/index.php?level=7&m_act%5Bwhat%5D=item&from=forma&m_act%5Bfind%5D="
Dim BaseAddress
BaseAddress = "http://www.kinopoisk.ru/"
Dim OriginalTitleFirst '������� ����. ��������, ����� �������
OriginalTitleFirst = False '���
Dim GetPosters '�������� ���� � ������� �������� � ���������� ������
GetPosters = True '��

Sub AnalyzePage()
 Dim LineNr, Line, Value
 Dim StartPos, EndPos
LineNr = SVC.FindLine("<title>���������� ������", 0)
If LineNr > -1 Then 
 Line = SVC.PageArr(LineNr)
 Value = SVC.MyVal(Line)
 If Value > 0 Then
  AddMoviesTitles (0)
 Else
  MTitles(0) = "���������: ������ �� �������."
 End If
Else
 '��� ����� �����
 LineNr = SVC.FindLine("<img src=""/images/film/", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  'id ��� ��������
  StartPos = InStr(Line, "<img src=""/images/film/") + 23
  EndPos = InStr(StartPos, Line, ".jpg")
  If EndPos > StartPos Then
   Value = Mid(Line, StartPos, EndPos - StartPos)
   If IsNumeric(Value) Then
    Redim MData(0): MData(0) = Value
   End If
  End If
  AnalyzeMoviePage 0
  SVC.PutToSVC
 End If
End If
End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line, Value 
 Dim MovieTitle, MovieAddress, MovieID
 Dim StartPos, EndPos, i, EndSearch

LineNr = SVC.FindLine("<!-- /���������� ������ -->", 0)
If LineNr > -1 Then
 EndSearch = LineNr
Else
 MTitles(0) = "���������: ������ �� �������."
 Exit Sub
End If

LineNr = SVC.FindLine("<!-- ���������� ������ -->", 0)
If LineNr > -1 Then
 i = 0
 Do
  LineNr = SVC.FindLine("/level/1/film/", LineNr)
  If LineNr > -1 Then
   If LineNr > EndSearch Then Exit Do
   Line = SVC.PageArr(LineNr)
   'id
   StartPos = InStr(Line, "level/1/film/") + 13
   EndPos = InStr(StartPos, Line, "/")
   If EndPos > StartPos Then
    MovieID = Mid(Line, StartPos, EndPos - StartPos)
    Redim Preserve MData(i): MData(i) = MovieID
   End If
   'url
   StartPos = InStr(Line, "level/1/film/")
   EndPos = InStr(StartPos, Line, """>")
   If EndPos > StartPos Then
    Value = Mid(Line, StartPos, EndPos - StartPos)
    MovieAddress = BaseAddress & Value
    ReDim Preserve MTitlesURL(i): MTitlesURL(i) = MovieAddress
    'title
    StartPos = EndPos + 2 
    EndPos = InStr(StartPos, Line, "</a>")
    If EndPos > StartPos Then
     Value = Mid(Line, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(Value)
     Redim Preserve MTitles(i): MTitles(i) = MovieTitle
    End If
   End If
   i = i + 1
  Else
   Exit Do
  End If
 LineNr = LineNr + 1
 Loop
End If
End Sub


Sub AnalyzeMoviePage(Ind)
 Dim Line, Value, Block
 Dim LineNr, StartNr, Counter, i
 Dim StartPos, EndPos
 Dim RusTitle, OrigTitle

'������� ��������
LineNr = SVC.FindLine("<tr><td><H1 style=", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 RusTitle = SVC.HTML2TEXT(Line)
End If
  
'������������ ��������
LineNr = SVC.FindLine("<span style=", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 OrigTitle = SVC.HTML2TEXT(Line)
End If

'��������
If (Len(RusTitle) <> 0) And (Len(OrigTitle) <> 0) Then 
 If OriginalTitleFirst = False Then 
  MTitle = RusTitle & " (" & OrigTitle & ")"
 Else
  MTitle = OrigTitle & " (" & RusTitle & ")"
 End If
ElseIf Len(RusTitle) <> 0 Then MTitle = RusTitle
ElseIf Len(OrigTitle) <> 0 Then MTitle = OrigTitle 
Else Exit Sub
End If
  
'���
LineNr = SVC.FindLine(">���<", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 Value = SVC.HTML2TEXT(Line)
 MYear = SVC.MyVal(Value)
End If
  
'������������ (������, ��������)
LineNr = SVC.FindLine(">������<", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 Value = SVC.HTML2TEXT(Line)
 MCountry = Replace(Value, "������ ", "")
 If MCountry = "-" Then MCountry = ""
End If

'�����c��
LineNr = SVC.FindLine(">��������<", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 Value = SVC.HTML2TEXT(Line)
 MDirector = Replace(Value, "�������� ", "")
 If MDirector = "-" Then MDirector = ""
End If

'����
LineNr = SVC.FindLine(">����<", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 Value = SVC.HTML2TEXT(Line)
 MGenre = Replace(Value, "���� ", "")
 If MGenre = "-" Then MGenre = ""
End If
  
'������
LineNr = SVC.FindLine(">� ������� �����:<", LineNr + 1)
If LineNr > -1 Then
 StartNr = LineNr
 LineNr = SVC.FindLine("</table>", LineNr + 1)
 If LineNr > StartNr Then
  Counter = LineNr - StartNr
 End If
End If
Value = ""
For i = 1 To Counter
 LineNr = StartNr + i
 Line = SVC.PageArr(LineNr)
 If InStr(Line, "<td height=15 align=right>") > 0 Then
  Value = Value & ", " & SVC.HTML2TEXT(Line)
 End If
Next
Value = Replace(Value, ", ...", "", 3)
MActors = Value

'��������
Block = SVC.GetBlockFrom(">� ���?<", "<table")
If Len(Block) <> 0 Then
 Value = SVC.HTML2TEXT(Block)
 Value = Right(Value, Len(Value) - 6)
 MDescription = Trim(Value)
End If
  
'�������
LineNr = SVC.FindLine(">IMDB:", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, ">IMDB:") 
 If StartPos > 0 Then
  EndPos = InStr(StartPos, Line, "<")
  If EndPos > StartPos Then
   Value = Mid(Line, StartPos, EndPos - StartPos)
   Value = SVC.HTML2TEXT(Value) '������� imdb
  End If
 Else
  Value = SVC.HTML2TEXT(Line) '����� ������� �������
 End If
  'Value = SVC.HTML2TEXT(Line) '������ ������� �������
  MRating = SVC.MyVal(Value)
End If

'�������� ���������
If MData(Ind) <> 0 Then
 MPicURL = BaseAddress & "images/film/" & MData(Ind) & ".jpg"
End If

If GetPosters Then
 Call GetPostersData(MData(Ind))
End If

End Sub ' AnalyzeMoviePage(Ind)

  
Sub GetPostersData (mdi)
 Dim LineNr, Line, Value, s, smPostersURL, Block
 Dim StartPos, EndPos
 Dim i
 
smPostersURL = BaseAddress & "level/17/film/" & mdi
SVC.ChangePage(smPostersURL)

LineNr = SVC.FindLine("alt=""�������� �������", 0)
i = 0

Do While LineNr > -1
 Line = SVC.PageArr(LineNr)
 s = "images/poster/sm_"
 StartPos = InStr(Line, s) 
 If StartPos > 0 Then
  StartPos = StartPos + Len(s) 
  EndPos = InStr(StartPos, Line, ".")
  If EndPos > StartPos Then
   Value = Mid(Line, StartPos, EndPos - StartPos)
   '���������
   Redim Preserve sPoster(i)
   sPoster(i) = BaseAddress & s & Value & ".jpg"
   'id
   Redim Preserve idPoster(i)
   idPoster(i) = SVC.MyVal(Line)
   '�����(������)
   Block = SVC.GetBlockFrom(Line, "</td>")
   Redim Preserve tPoster(i)
   tPoster(i) = Left(SVC.HTML2TEXT(Block), 18)
  End If
 End If
 LineNr = SVC.FindLine("alt=""�������� �������", LineNr + 1)
 i = i + 1
Loop
End Sub 'GetPostersData (mdi)


Sub GetPoster (ID)
 '��� ����� �� ���� ������ �� svc
 Dim PosterURL, LineNr, Line
 Dim StartPos, EndPos

If ID <> 0 Then
 PosterURL = BaseAddress & "picture/" & ID
 SVC.ChangePage(PosterURL)

 LineNr = SVC.FindLine("images/poster/", 0)   '10x 2 SVCUser, 2008
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  StartPos = InStr(Line, "images/poster/") 
  If StartPos > 0 Then
   EndPos = InStr(StartPos, Line, "'")
   If EndPos > StartPos Then
    MPicURL = BaseAddress & Mid(Line, StartPos, EndPos - StartPos)
   End If
  End If
 End If
End If

End Sub 'GetPoster (Ind)
