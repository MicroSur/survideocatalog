'
'ShareReactor.ru html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2006-2008.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://sharereactor.ru/cgi-bin/mzsearch.cgi?search="
Dim BaseAddress
BaseAddress = "http://sharereactor.ru/"
Dim OriginalTitleFirst
OriginalTitleFirst = False 'or True

Sub AnalyzePage()
 Dim LineNr 
LineNr = SVC.FindLine("�� ������ ������� ��������", 0)
If LineNr > -1 Then 
 AddMoviesTitles (LineNr)
Else
 MTitles(0) = "ShareReactor: ������ �� �������."
End If
End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line
 Dim MovieTitle, MovieAddress, MovieID
 Dim StartPos, EndPos, i
 Dim TotalMovies

Line = SVC.PageArr(LineNr)
TotalMovies = SVC.MyVal(Line)
 
LineNr = SVC.FindLine("CLASS=""historytext""", LineNr)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)

 StartPos = 1
 For i = 0 To TotalMovies - 1
  StartPos = InStr(StartPos, Line, "<A HREF=""/movies/")
  If StartPos > 0 Then
   EndPos = InStr(StartPos, Line, "</A>")
   If EndPos > StartPos Then
    MovieTitle = Mid(Line, StartPos, EndPos - StartPos)
    MovieID = SVC.MyVal(MovieTitle)
    MovieAddress = BaseAddress & "movies/" & MovieID
    MovieTitle = SVC.HTML2TEXT(MovieTitle)
    Redim Preserve MTitles(i): MTitles(i) = MovieTitle
    ReDim Preserve MTitlesURL(i): MTitlesURL(i) = MovieAddress
   End If
  End If
  StartPos = EndPos + 4 '</A>
 Next
End If
End Sub

Sub AnalyzeMoviePage(Ind)
 Dim Line, Value
 Dim LineNr, i
 Dim BeginPos, EndPos
 Dim temp
 Dim PicAlias
 Dim RusTitle, OrigTitle

'������� ��������
LineNr = SVC.FindLine("<td><H1>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "<H1>")
 If BeginPos > 0 Then BeginPos = BeginPos + 4
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos - 22)
  RusTitle = SVC.HTML2TEXT(Value)
  PicAlias = RusTitle
 End If
End If
  
'������������ ��������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
  OrigTitle = SVC.HTML2TEXT(Value)
 End If
End If
   
'��������
If (Len(RusTitle) <> 0) And (Len(OrigTitle) <> 0) Then 
 If OriginalTitleFirst = False Then 
  MTitle = RusTitle & " (" & OrigTitle & ")"
 Else
  MTitle = OrigTitle & " (" & RusTitle & ")"
 End If
ElseIf Len(RusTitle) <> 0 Then MTitle = RusTitle
ElseIf Len(OrigTitle) <> 0 Then MTitle = OrigTitle 
Else Exit Sub
End If
  
'��� ������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
  EndPos = Len(Line)
  If (EndPos > BeginPos) And (BeginPos > 0) Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
   MYear = SVC.HTML2TEXT(Value)
  End If
End If
  
'����
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos +1)
  MGenre = SVC.HTML2TEXT(Value)
 End If
End If

'�����������������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
  
'�����c��
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
  MDirector = SVC.HTML2TEXT(Value)
 End If
End If
  
'������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
  MActors = SVC.HTML2TEXT(Value)
 End If
End If

'��������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
  MDescription = SVC.HTML2TEXT(Value)
 End If
End If
  
'������������ (������, ��������)
'LineNr = SVC.FindLine("&nbsp", LineNr + 1)
'If LineNr > -1 Then
' Line = SVC.PageArr(LineNr)
' BeginPos = InStr(Line, "</B>")
' If BeginPos > 0 Then BeginPos = BeginPos + 5
' EndPos = Len(Line)
' If (EndPos > BeginPos) And (BeginPos > 0) Then
'  Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
'  MCountry = SVC.HTML2TEXT(Value)
' End If
'End If

'����
'LineNr = SVC.FindLine("&nbsp", LineNr + 1)
'If LineNr > -1 Then
' Line = SVC.PageArr(LineNr)
' BeginPos = InStr(Line, "</B>")
' If BeginPos > 0 Then BeginPos = BeginPos + 5
' EndPos = Len(Line)
' If (EndPos > BeginPos) And (BeginPos > 0) Then
'  Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
'  MLang = SVC.HTML2TEXT(Value)
' End If
'End If

'����������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)

'�������������
LineNr = SVC.FindLine("&nbsp", LineNr + 1)

'�������
LineNr = SVC.FindLine("&nbsp", LineNr + 2)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "</B>")
 If BeginPos > 0 Then BeginPos = BeginPos + 5
 EndPos = InStr(BeginPos, Line, "/")
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MRating = SVC.HTML2TEXT(Value)
 End If
End If

'��������
MPicURL = ""

LineNr = SVC.FindLine("pictures/", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "http:/")
 EndPos = InStr(Line, ".jpg")
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  MPicURL = Mid(Line, BeginPos, EndPos - BeginPos + 4)
 Else
  EndPos = InStr(Line, ".gif")
  If (EndPos > BeginPos) And (BeginPos > 0) Then
   MPicURL = Mid(Line, BeginPos, EndPos - BeginPos + 4)
  End If
 End If
End If

If MPicURL = "" Then
 LineNr = SVC.FindLine("/images/", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "http:/")
  EndPos = InStr(Line, ".jpg")
  If (EndPos > BeginPos) And (BeginPos > 0) Then
   MPicURL =  Mid(Line, BeginPos, EndPos - BeginPos + 4)
  Else
   EndPos = InStr(Line, ".gif")
   If (EndPos > BeginPos) And (BeginPos > 0) Then
    MPicURL = Mid(Line, BeginPos, EndPos - BeginPos + 4)
   End If
  End If
 End If
End If
        
If MPicURL = "" Then
 LineNr = SVC.FindLine("covers/", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "SRC='/") + 6
  EndPos = InStr(Line, ".jpg")
  If (EndPos > BeginPos) And (BeginPos > 6) Then
   MPicURL = BaseAddress & Mid(Line, BeginPos, EndPos - BeginPos + 4)
  Else
   BeginPos = InStr(Line, "SRC='") + 5
   If (EndPos > BeginPos) And (BeginPos > 0) Then
    MPicURL = Mid(Line, BeginPos, EndPos - BeginPos + 4)
   End If
  End If
 End If
End If

If MPicURL = "" Then
 LineNr = SVC.FindLine("alt='" & PicAlias, 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "SRC='/") + 6
  EndPos = InStr(Line, ".jpg")
  If (EndPos > BeginPos) And (BeginPos > 6) Then
   MPicURL = BaseAddress & Mid(Line, BeginPos, EndPos - BeginPos + 4)
  Else
   BeginPos = InStr(Line, "SRC='") + 5
   If (EndPos > BeginPos) And (BeginPos > 0) Then
    MPicURL = Mid(Line, BeginPos, EndPos - BeginPos + 4)
   End If
  End If
 End If
End If

End Sub
  
  


