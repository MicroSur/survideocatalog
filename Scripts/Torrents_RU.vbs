'
'torrents.ru forum html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2008.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://torrents.ru/forum/search.php?f[]=7&nm="
Dim BaseAddress
BaseAddress = "http://torrents.ru/forum/"


Sub AnalyzePage()
 Dim LineNr 

LineNr = SVC.FindLine("����������� ������:", 0)
If LineNr > -1 Then 
 AddMoviesTitles (LineNr)
 Exit Sub
End If

LineNr = SVC.FindLine("���������� ��� ��� ��������� �� �������", 0)
If LineNr > -1 Then 
 MTitles(0) = "torrents.ru: ������ �� �������."
 Exit Sub
End If

End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line, Value
 Dim MovieTitle, MovieAddress, MovieID
 Dim StartPos, EndPos, i
 Dim TotalMovies

Line = SVC.PageArr(LineNr)
TotalMovies = SVC.MyVal(SVC.HTML2TEXT(Line))

For i = 0 To TotalMovies - 1
 LineNr = SVC.FindLine(""" class=""topictitle"">", LineNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)

  StartPos = InStr(1, Line, "<a href=""./") + 11
  If StartPos > 11 Then
   EndPos = InStr(StartPos, Line, """ class=""topictitle"">")
   If EndPos > StartPos Then
    MovieAddress = BaseAddress & Mid(Line, StartPos, EndPos - StartPos)
   End If
  
   StartPos = InStr(StartPos, Line, "topictitle"">") + 12
   If StartPos > 12 Then
    EndPos = InStr(StartPos, Line, "</a>")
    If EndPos > StartPos Then
     Value = Mid(Line, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(Value)
    End If
    Redim Preserve MTitles(i): MTitles(i) = MovieTitle
    ReDim Preserve MTitlesURL(i): MTitlesURL(i) = MovieAddress
   End If
  End If

  LineNr = LineNr + 1
 End If
Next

End Sub



Sub AnalyzeMoviePage(Ind)
 Dim Line, Value
 Dim LineNr, i, LineNrFilm
 Dim BeginPos, EndPos
 Dim temp
 Dim PicAlias
 Dim RusTitle, OrigTitle

'��������                                    
LineNr = SVC.FindLine("<span style=", 0)
If LineNr > -1 Then
 LineNrFilm = LineNr
 Line = SVC.PageArr(LineNr)
 BeginPos = 1
 EndPos = InStr(Line, "</span>")
 If EndPos > BeginPos Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MTitle = SVC.HTML2TEXT(Value)
 End If
End If
  
'��� ������
LineNr = SVC.FindLine("��� �������</span>", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "��� �������</span>")
 If BeginPos > 0 Then 
  EndPos = Len(Line)
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MYear = SVC.MyVal(Value)
  End If
 End If
End If
  
'������
LineNr = SVC.FindLine("������</span>: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "������</span>: ")
 If BeginPos > 0 Then BeginPos = BeginPos + 15
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MCountry = SVC.HTML2TEXT(Value)
 End If
End If

'����
LineNr = SVC.FindLine("����</span>: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "����</span>: ")
 If BeginPos > 0 Then BeginPos = BeginPos + 13
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos +1)
  Value = SVC.HTML2TEXT(Value)
  Value = Replace(Value, ".", "")
  MGenre = Value
 End If
End If

'�����������������
'LineNr = SVC.FindLine("�����������������</span>: ", LineNrFilm)

'�������
LineNr = SVC.FindLine("�������</span>: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "�������</span>: ")
 If BeginPos > 0 Then BeginPos = BeginPos + 16
 EndPos = Len(Line)
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MLang = SVC.HTML2TEXT(Value)
 End If
End If

'������� ��������: ��� ?
  
'�����c��
LineNr = SVC.FindLine("��������</span>: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "��������</span>: ")
 If BeginPos > 0 Then BeginPos = BeginPos + 17
 EndPos = InStr(BeginPos, Line, "</span>")
 If (EndPos > BeginPos) And (BeginPos > 0) Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MDirector = SVC.HTML2TEXT(Value)
 End If
End If
  
'������
LineNr = SVC.FindLine("� �����</span>: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.GetBlockFrom("� �����</span>:", "<span class=""post-br"">")
 If Len(Line) <> 0 Then
  Value = SVC.HTML2TEXT(Line)
  Value = Replace(Value, "� ����� : ", "")
  MActors = Value
 End If
End If

'��������
LineNr = SVC.FindLine("��������</span>:", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.GetBlockFrom("��������</span>:", "<span class=""post-br"">")
 If Len(Line) <> 0 Then
  Value = SVC.HTML2TEXT(Line)
  Value = Replace(Value, "�������� : ", "")
  MDescription = Value
 End If
End If

'�������
LineNr = SVC.FindLine("User Rating: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "User Rating: ")
 If BeginPos > 0 Then
  EndPos = InStr(BeginPos, Line, "/")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MRating = SVC.MyVal(Value)
  End If
 End If
End If

'��������
LineNr = SVC.FindLine("��������</span>: ", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "��������</span>: ")
 If BeginPos > 0 Then
  BeginPos = BeginPos + 17
  EndPos = InStr(BeginPos, Line, "<br")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MOther = SVC.HTML2TEXT(Value)
  End If
 End If
End If

'��������
LineNr = SVC.FindLine("<var class=""postImg", LineNrFilm)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "title=""")
 If BeginPos > 0 Then 
  BeginPos = BeginPos + 7 
  EndPos = InStr(BeginPos, Line, """>")
  If EndPos > BeginPos Then
   MPicURL = Mid(Line, BeginPos, EndPos - BeginPos)
  End If
 End If
End If


End Sub
  
  


