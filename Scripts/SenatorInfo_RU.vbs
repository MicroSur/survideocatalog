'
'SENATORINFO html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2006-2008.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://www.senatorinfo.com/index.php?search=1&s_name="

Dim BaseAddress
BaseAddress = "http://www.senatorinfo.com/"

Dim PostMethod
'True - POST, False - GET 
PostMethod = True

Dim OriginalTitleFirst
OriginalTitleFirst = False 'or True

Sub AnalyzePage()
 Dim LineNr 
LineNr = SVC.FindLine("<b class=""mi"">", 0)
If LineNr > -1 Then
 AddMoviesTitles (LineNr)
Else
 MTitles(0) = "SenatorInfo: ������ �� �������."
End If
End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line, Value
 Dim MovieTitle, MovieAddress
 Dim StartPos, EndPos, i

Line = SVC.GetBlockFrom("<b class=""mi"">")
Do
 StartPos = InStr(Line, """ href=""index.php?film=") + 8
 If StartPos > 8 Then
  EndPos = InStr(StartPos, Line, """>")
  If EndPos > StartPos Then
   MovieAddress = Mid(Line, StartPos, EndPos - StartPos)
   StartPos = InStr(Line, "<b class=""mi"">")
   If StartPos > 0 Then
    EndPos = InStr(StartPos, Line, "</A>")
    If EndPos > StartPos Then
     Value = Mid(Line, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(Value)
     Redim Preserve MTitles(i): MTitles(i) = MovieTitle
     ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
     i = i + 1
     Line =  Mid(Line, EndPos, Len(Line))
    End If
   Else
    Exit Do
   End If
  End If
 Else
  Exit Do
 End If
Loop 
End Sub

Sub AnalyzeMoviePage(Ind)
 Dim Line, Value
 Dim LineNr, BlockNr
 Dim BeginPos, EndPos
 Dim RusTitle, OrigTitle
 Dim Block

LineNr = SVC.FindLine("��������� ���������� � ������:", 0)
If LineNr > -1 Then
 BlockNr = LineNr

 '��������
 LineNr = SVC.FindLine("<p><b>��������: </b>", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, "<p><b>��������: </b>") + 20
  EndPos = InStr(BeginPos, Line, "</p>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   RusTitle = SVC.HTML2TEXT(Value)
  End If
 End If

 '���� �������� 
 LineNr = SVC.FindLine("<p><b>������������ ��������: </b>", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, "<p><b>������������ ��������: </b>") + 33
  EndPos = InStr(BeginPos, Line, "</p>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   OrigTitle = SVC.HTML2TEXT(Value)
  End If
 End If

 '��������
 If (Len(RusTitle) <> 0) And (Len(OrigTitle) <> 0) Then
  If OriginalTitleFirst = False Then
   MTitle = RusTitle & " (" & OrigTitle & ")"
  Else
   MTitle = OrigTitle & " (" & RusTitle & ")"
  End If
 ElseIf Len(RusTitle) <> 0 Then MTitle = RusTitle
 ElseIf Len(OrigTitle) <> 0 Then MTitle = OrigTitle
 Else Exit Sub
 End If

 '���
 LineNr = SVC.FindLine (">��� �������:", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  MYear = SVC.MyVal(Line)
 End If

 '����
 LineNr = SVC.FindLine (">����:", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr + 1) 
  Block = SVC.GetBlockFrom(Line, "</p>")
  Value = Replace (Block,"</A>", ", ")
  MGenre = SVC.HTML2TEXT(Value)
 End If

 '���
 LineNr = SVC.FindLine (">��������:", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">��������:") + 10
  EndPos = Len(Line)
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MDirector = SVC.HTML2TEXT(Value)
  End If
 End If

 '����
 LineNr = SVC.FindLine (">� �����:", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">� �����:") + 9
  EndPos = Len(Line)
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "� ������", "")
   MActors = SVC.HTML2TEXT(Value)
  End If
 End If

 '�����
 LineNr = SVC.FindLine (">��������:", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">��������:") + 10
  EndPos = Len(Line)
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
   MDescription = SVC.HTML2TEXT(Value)
  End If
 End If

 '������
 LineNr = SVC.FindLine (">������:", BlockNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">������:") + 8
  EndPos = Len(Line)
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
   Value = Replace(Value,"/",",")
   MCountry = SVC.HTML2TEXT(Value)
  End If
 End If

' '����
' LineNr = SVC.FindLine (">����:", BlockNr)
' If LineNr > -1 Then
'  Line = SVC.PageArr(LineNr) 
'  BeginPos = InStr(Line, ">����:") + 6
'  EndPos = Len(Line)
'  If EndPos > BeginPos Then
'   Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
'   MLang = SVC.HTML2TEXT(Value)
'  End If
' End If

' '������� ������
' LineNr = SVC.FindLine (">������� imdb.com:", BlockNr)
' If LineNr > -1 Then
'  Line = SVC.PageArr(LineNr)
'  BeginPos = InStr(Line, ">������� imdb.com:") + 18
'  EndPos = InStr(BeginPos, Line, "&nbsp;/")
'  If EndPos > BeginPos Then
'   Value = Mid(Line, BeginPos, EndPos - BeginPos)
'   MRating = SVC.MyVal(Value)
'  End If
' End If

 '��������
 LineNr = SVC.FindLine ("<IMG id=""cover", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, "src=""") + 5
  EndPos = InStr(BeginPos, Line, "jpg") + 3
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   If InStr(Value,"http") Then 
    MPicURL = Value
   Else
    MPicURL = Left(BaseAddress,Len(BaseAddress)-1) & Value
   End If
  End If
 End If

End If 'LineNr > -1
End Sub
