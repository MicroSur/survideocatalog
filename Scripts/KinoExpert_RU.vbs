'
'KinoExpert html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2006-2007.
'
'NoCastFlag = False (����� ��������� ( ...���� ������ ))
'NoCastFlag = True (����� ���������)

Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://www.kinoexpert.ru/index.asp?comm=1&kw="

Dim BaseAddress
BaseAddress = "http://www.kinoexpert.ru/"

Dim NoCastFlag
NoCastFlag = True

Sub AnalyzePage()
 Dim LineNr 
LineNr = SVC.FindLine("<br>�� ������ �������", 0)
If LineNr > -1 Then 
 If SVC.FindLine("������ �� ���� ������� ��", LineNr) > -1 Then
  MTitles(0) = "KinoExpert: ������ �� �������."   
 Else
  AddMoviesTitles (LineNr)
 End If
End If
End Sub

Sub DelLastDot(ByRef Value)
 If Len(Value) > 1 Then
  If Right(Value, 1) = "." Then Value = Left(Value, Len(Value) - 1)
 End If
End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line
 Dim MovieTitle, MovieAddress
 Dim StartPos, EndPos, i
 Dim TotalLines

TotalLines = SVC.FindLine("</html>", 0)

LineNr = SVC.FindLine("</tr><tr><td><a", LineNr)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 EndPos = 1
 Do
  StartPos = InStr(EndPos, Line, "index.asp?comm=4")
  If StartPos > 0 Then
   EndPos = InStr(StartPos, Line, ">")
   If EndPos > StartPos Then
    MovieAddress = Mid(Line, StartPos, EndPos - StartPos -1)
    If InStr(MovieAddress, "#") > 0 Then MovieAddress = Left(MovieAddress, InStr(MovieAddress, "#") - 1)
    StartPos = EndPos +1
    EndPos = InStr(StartPos, Line, "</a>")
    If EndPos > StartPos Then
     MovieTitle = Mid(Line, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(MovieTitle)
     Redim Preserve MTitles(i): MTitles(i) = MovieTitle
     ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
     i = i + 1
    End If
   End If
  Else
   If InStr(Line, "</table>") Then Exit Do
   LineNr = LineNr + 1
   If LineNr > TotalLines Then Exit Do
   Line = SVC.PageArr(LineNr)
   EndPos = 1
  End If
 Loop 
End If
End Sub


Sub AnalyzeMoviePage(Ind)
 Dim Line, Value
 Dim LineNr, i
 Dim BeginPos, EndPos
 Dim ln1, ln2
 Dim temp, block

'��������
LineNr = SVC.FindLine("�������� ���� �������", 0)
If LineNr < 0 Then LineNr = SVC.FindLine("������ �� ����", 0)
If LineNr > -1 Then 
 LineNr = SVC.FindLine("<font size=""4""", LineNr)
 If LineNr < 0 Then LineNr = SVC.FindLine("<b><h1>", LineNr)
 If LineNr > -1 Then 
  Line = SVC.PageArr(LineNr)
  BeginPos = 1
  EndPos = InStr(BeginPos, Line, "<font size=""2""")
  If (EndPos > BeginPos) Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "�������� ���� �������", "")
   MTitle = SVC.HTML2TEXT(Value)
  End If
 End If
End If

'table
'ln1 = SVC.FindLine("����.", LineNr)
'ln2 = SVC.FindLine("<table", ln1)
'If (ln1 > -1) And (ln2 > -1) And (ln2 > ln1) Then
' For i = ln1 + 1 To ln2
'  block = block & SVC.PageArr(i)
' Next
'End If
block = SVC.GetBlockFrom("����.","<table")

If len(block) <> 0 Then
 '������
 If EndPos > 0 Then 
  Value = Mid(Line, EndPos, Len(Line) - EndPos)
  temp = SVC.HTML2TEXT(Value) 
 End If
 '������������
 BeginPos = InStr(block, "<tr")
 If BeginPos > 0 Then
  EndPos = InStr(BeginPos, block, "</td>")
  If EndPos > BeginPos Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   Value = SVC.HTML2TEXT(Value)
   If (Len(temp) > 0) And (Len(Value) > 0) Then
    MCountry = Value & ", " & temp
   Else 
    If Len(Value) > 0 Then MCountry = Value
    If Len(temp) > 0 Then MCountry = temp
   End If
  End If
 End If

 '���
 BeginPos = InStr(EndPos, block, "<td")
 If BeginPos > 0 Then
  EndPos = InStr(BeginPos, block, "</td>")
  If EndPos > BeginPos Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   MYear = SVC.HTML2TEXT(Value)
  End If
 End If

 '����
 BeginPos = InStr(EndPos, block, "<td")
 If BeginPos > 0 Then
  EndPos = InStr(BeginPos, block, "</td>")
  If EndPos > BeginPos Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   MGenre = SVC.HTML2TEXT(Value)
  End If
 End If
  
 '����
 BeginPos = InStr(EndPos, block, "� �����") 
 If BeginPos > 0 Then
  BeginPos = BeginPos + 7
  EndPos = InStr(BeginPos, block, "<br>")
  If (EndPos > BeginPos) Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "� �����", "")
   If NoCastFlag Then
    Value = Replace(Value, "(<i>", "<")
    Value = Replace(Value, "/i>)", ">")
   End If                     
   Value = Replace(Value, Chr(187), "")
   Value = Replace(Value, Chr(171), "")
   Value = SVC.HTML2TEXT(Value)
   Value = Replace(Value, "[ ]:", "")
   Value = Trim(Value)
   DelLastDot Value
   MActors = Value
  End If
 End If

 '���
 BeginPos = InStr(EndPos, block, "��������:") 
 If BeginPos > 0 Then
  BeginPos = BeginPos + 9 
  EndPos = InStr(BeginPos, block, "<br>")
  If (EndPos > BeginPos) Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   Value = SVC.HTML2TEXT(Value)
   DelLastDot Value
   MDirector = Value
  End If 
 End If 

 '����
 temp = ""
 BeginPos = InStr(EndPos, block, "������� � ����������:") 
 If BeginPos > 0 Then 
  BeginPos = BeginPos + 21 
  EndPos = InStr(BeginPos, block, "������� ����������:")
  If (EndPos > BeginPos) Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   temp = SVC.HTML2TEXT(Value)
  End If
 End If

 '�����
 BeginPos = InStr(EndPos, block, "������� ����������:")
 If BeginPos > 0 Then
  BeginPos = BeginPos + 19 
  EndPos = InStr(BeginPos, block, "</div><BR>")
  If (EndPos > BeginPos) And (BeginPos > 0) Then
   Value = Mid(block, BeginPos, EndPos - BeginPos)
   If Len(temp) <> 0 Then
    MDescription = SVC.HTML2TEXT(Value) & CHR(13) & CHR(10) & temp
   Else
    MDescription = SVC.HTML2TEXT(Value)
   End If
  End If
 End If

 '�������� ���

End If 'len(block) <> 0

End Sub