'
'nashekino.ru html pages parser.
'VbScript adaptation by Sur for Sur Video Catalog. 2005-2008.
'
'NoCastFlag = False (���� ������� - "���� ���������� ����� / ����� ���������� �������)
'NoCastFlag = True (���� �������)

Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
ReDim MTitles(0)
Dim url, BaseAddress

'url = "http://www.nashekino.ru/cgi-bin/find.cgi?t=0&sval="
url = "http://www.nashekino.ru/data.find?t=0&sval="
BaseAddress = "http://www.nashekino.ru/"

Dim NoCastFlag
NoCastFlag = True


Sub AnalyzePage()
  Dim LineNr
  Dim TextBlock

'LineNr = SVC.FindLine("<hr width=""100%"" size=1 color=""black"">", 0)
LineNr = SVC.FindLine("�� ��������� �������� ���������� ���", 0)
If LineNr > -1 Then
 MTitles(0) = "��������: ������ �� �������."
 Exit Sub
Else
 LineNr = SVC.FindLine(">�������", 0)
 If LineNr > -1 Then 
  AddMoviesTitles (LineNr)
 End If
End If
End Sub

Sub AnalyzeMoviePage(Ind)
 Dim Value, TextBlock, Line
 Dim LineNr, MovieLength, BeginPos, EndPos

TextBlock = SVC.GetBlockFrom("><H1>")
EndPos = InStr(TextBlock, "</td>")
If EndPos > 2 Then
 TextBlock = Mid(TextBlock, 1, EndPos - 1)
End If

If Len(TextBlock) > 0 Then
 'Title
 BeginPos = 1
 EndPos = InStr(TextBlock, "</a>")
 If EndPos > BeginPos Then
  Value = Mid(TextBlock, BeginPos, EndPos - BeginPos)
  MTitle = SVC.HTML2TEXT(Value)
  TextBlock = Right(TextBlock, Len(TextBlock) - EndPos + 1)
  BeginPos = InStr(TextBlock, "������ ��������:")
  If BeginPos > 0 Then 
   TextBlock = Right(TextBlock, Len(TextBlock) - BeginPos + 1)
  End If
 End If

 'Year
 EndPos = InStr(TextBlock, "���.") - 1
 BeginPos = EndPos - 4 
 If EndPos > BeginPos And BeginPos > 0 Then
  MYear = Mid(TextBlock, BeginPos, EndPos - BeginPos)
  TextBlock = Right(TextBlock, Len(TextBlock) - EndPos + 1)
 End If
End If
    
'Director
TextBlock = SVC.GetBlockFrom("��������(�): ")
If Len(TextBlock) > 0 Then
 BeginPos = 14
 EndPos = InStr(TextBlock, "<br>")
 If EndPos > BeginPos Then
  Value = Mid(TextBlock, BeginPos, EndPos - BeginPos)
  MDirector = SVC.HTML2TEXT(Value)
 End If
End If

'Actors
TextBlock = SVC.GetBlockFrom("�����(�): ")
If Len(TextBlock) > 0 Then
 BeginPos = 11
 EndPos = InStr(TextBlock, "<br>")
 If EndPos > BeginPos Then
  Value = Mid(TextBlock, BeginPos, EndPos - BeginPos)
  If NoCastFlag Then
   Value = Replace(Value,"ab10""> ","")
  End If
  MActors = SVC.HTML2TEXT(Value)
 End If
End If

'Studio
LineNr = SVC.FindLine("������������:", 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "������������:") + 13
 EndPos = Len(Line)
 If EndPos > BeginPos Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MCountry = SVC.HTML2TEXT(Value)
 End If
Else
 MCountry = "������"
End If

'Description
TextBlock = SVC.GetBlockFrom("� ������:")
If Len(TextBlock) > 0 Then
 BeginPos = InStr(TextBlock, "<a class=")
 EndPos = InStr(TextBlock, "<hr")
 If EndPos > BeginPos Then
  Value = Mid(TextBlock, BeginPos, EndPos - BeginPos)
  MDescription = SVC.HTML2TEXT(Value)
 End If
End If

'����
LineNr = SVC.FindLine("<img name=""foto", 1)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "src=""/") + 6
 EndPos = InStr(BeginPos, Line, "jpg") + 3
 If EndPos > BeginPos Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  MPicURL = BaseAddress & Value
 End If
End If

End Sub



Sub AddMoviesTitles(LineNr)
  Dim Line, MovieTitle, MovieAddress, cgibin, Temp
  Dim StartPos, EndPos, i, n

Line = SVC.PageArr(LineNr)
StartPos = InStr(Line, ">�������")
Temp = Right(Line, Len(Line) - StartPos)
'������� �����
n = SVC.MyVal(Temp)

For i = 0 to n - 1
LineNr = SVC.FindLine("/data.movies?id=", LineNr)
If LineNr > -1 Then 
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "data.movies?id=")
 EndPos = InStr(StartPos, Line, """>")
 If EndPos > StartPos Then
  MovieAddress = Mid(Line, StartPos, EndPos - StartPos)
  MovieTitle = SVC.HTML2TEXT(Line)
  ReDim Preserve MTitles(i): MTitles(i) = MovieTitle
  ReDim Preserve MTitlesURL(i)
  MTitlesURL(i) = BaseAddress & MovieAddress
 End If
End If
LineNr = LineNr + 1
Next

End Sub
