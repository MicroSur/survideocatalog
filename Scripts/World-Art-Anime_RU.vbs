'
'World-art anime html pages parser. 
'VbScript adaptation by Shadowrunnner Joshua for Sur Video Catalog. 2007.
'Correction by Sur 2008.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Redim MTitlesURL(0)
Redim MData(0)
Dim url
url = "http://world-art.ru/search.php?global_sector=animation&name="

Dim BaseAddress
BaseAddress = "http://www.world-art.ru/"

Sub AnalyzePage()
 Dim LineNr, Line
 Dim StartPos, EndPos
 
 LineNr = SVC.FindLine("<b>������ ", 0)
 If LineNr > -1 Then
  AddMoviesTitles (LineNr)
 Else
  LineNr = SVC.FindLine("������ �� �������", 0)
  If LineNr > -1 Then
   MTitles(0) = "WA Anime: ������ �� �������."
  Else

  LineNr = SVC.FindLine("not_connect.htm", 0)
  If LineNr > -1 Then
   MTitles(0) = "WA Anime: ������ �� ��������."
   Exit Sub
  End If

   MTitles(0) = "1 ������"
   LineNr = SVC.FindLine("animation.php?id=", 0)
   Line = SVC.PageArr(LineNr)
   StartPos = InStr(1, Line, "animation/")
   EndPos = InStr(StartPos, Line, "'")
   MTitlesURL(0) =BaseAddress &  Mid(Line, StartPos, EndPos - 24)
   MData(0) = SVC.MyVal(MTitlesURL(0))
  End If
 End If
End Sub

Sub DelLastDot(ByRef Value)
 If Len(Value) > 1 Then
  If Right(Value, 1) = "." Then Value = Left(Value, Len(Value) - 1)
 End If
End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line
 Dim MovieTitle, MovieAddress
 Dim StartPos, EndPos, i

 Line = SVC.PageArr(LineNr)
 EndPos = 1
 Do
  StartPos = InStr(EndPos, Line, "animation/animation.php?")
  If StartPos > 0 Then
   EndPos = InStr(StartPos, Line, ">")
   If EndPos > StartPos Then
    MovieAddress = Mid(Line, StartPos, EndPos - StartPos -1)
    If InStr(MovieAddress, """ class") > 0 Then MovieAddress = Left(MovieAddress, InStr(MovieAddress, """ class") - 1)
    StartPos = EndPos +1
    EndPos = InStr(StartPos, Line, "</a>")
    If EndPos > StartPos Then
     MovieTitle = Mid(Line, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(MovieTitle)
     Redim Preserve MTitles(i): MTitles(i) = MovieTitle
     ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
     ReDim Preserve MData(i): MData(i) = SVC.MyVal(MTitlesURL(i))
     i = i + 1
    End If
   End If
  Else
   Exit Do
  End If
 Loop 

End Sub

Sub AnalyzeMoviePage(Ind)
 Dim Value, TextBlock, Line, NextURL
 Dim LineNr, MovieLength, BeginPos, EndPos
 
'Title
TextBlock = SVC.GetBlockFrom("<title>")
EndPos = InStr(TextBlock, "</title>")
Value = Mid(TextBlock, 15, EndPos)
MTitle = SVC.HTML2TEXT(Value)
 
'Year
TextBlock = SVC.GetBlockFrom("year=")
EndPos = InStr(TextBlock, ">")
Value = Mid(TextBlock, 6, EndPos-7)
MYear = SVC.HTML2TEXT(Value)
 
'Genre
TextBlock = SVC.GetBlockFrom("<a href='list.php?genre")
EndPos = InStr(TextBlock, "<br>")
Value = Mid(TextBlock, 1, EndPos)
MGenre = SVC.HTML2TEXT(Value)
 
'Country
TextBlock = SVC.GetBlockFrom("<a href='../country.php")
EndPos = InStr(TextBlock, "<br>")
Value = Mid(TextBlock, 1, EndPos-0)
MCountry = SVC.HTML2TEXT(Value)
 
'Director
TextBlock = SVC.GetBlockFrom("<a href = ""../people.php?")
'EndPos = InStr(TextBlock, "<br>") '� �����������
EndPos = InStr(TextBlock, "</")
Value = Mid(TextBlock, 1, EndPos-0)
MDirector = SVC.HTML2TEXT(Value)
 
'Rating
TextBlock = SVC.GetBlockFrom("������� ����")
EndPos = InStr(TextBlock, "<br>")
Value = Mid(TextBlock, 1, EndPos-0)
MRating = SVC.MyVal(Value) 
'MRating = SVC.HTML2TEXT(Value)
 
'Picture
TextBlock = SVC.GetBlockFrom("<img src='img/")
If Len(TextBlock)>0 Then
 EndPos = InStr(TextBlock, "jpg")
 If EndPos > 1 Then
  Value = SVC.MyVal(Mid(TextBlock, 1, EndPos))
 End If
End If
MPicURL = BaseAddress & "animation/img/" & Value & "/" & MData(Ind) & "/1.jpg"

'Comments
TextBlock = SVC.GetBlockFrom("����������� �� ���� �����:")
EndPos = InStr(TextBlock, "<a href='users_review_all.php?")
Value = Mid(TextBlock, 1, EndPos-0)
MDescription = SVC.HTML2TEXT(Value)
If Len(MDescription) = 0 Then 
 TextBlock = SVC.GetBlockFrom("������� ����������:")
 EndPos = InStr(TextBlock, "</table><br>")
 Value = Mid(TextBlock, 20, EndPos-0)
 MDescription = SVC.HTML2TEXT(Value)
End If


'��� � ����������
TextBlock = SVC.GetBlockFrom("���<")
EndPos = InStr(TextBlock, "<br>")
Value = Mid(TextBlock, 1, EndPos)
MOther = SVC.HTML2TEXT(Value)

'�������� �� ������ ��������
LineNr = SVC.FindLine ("animation_full_production.php?id=", 0)
If LineNr > -1 Then 
 NextURL = BaseAddress & "animation/animation_full_production.php?id=" & MData(ind)
 SVC.ChangePage(NextURL) 
  LineNr = SVC.FindLine (">������������:<", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">������������:<") + 14
  EndPos = InStr(BeginPos, Line, "</table>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "<tr>",",")
   Value = SVC.HTML2TEXT(Value)
   Value = Replace(Value, " ,",",")
   IF Len(MCountry) = 0 Then
    MCountry = Value
   Else
    MCountry = MCountry & ", " & Value
   End If
  End If
 End If
End If

End Sub
