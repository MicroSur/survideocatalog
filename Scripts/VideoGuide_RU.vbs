'
'VideoGuide.ru html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2005-2007.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://www.videoguide.ru/find.asp?Search=Simple&types=film&titles="
Dim BaseAddress
BaseAddress = "http://www.videoguide.ru/"

Sub AnalyzePage()
  Dim LineNr, i
  Dim Line, TextBlock

If SVC.FindLine("����� ����� �� ������", 0) > -1 Then
 MTitles(0) = "VideoGuide: ������ �� �������."
 Exit Sub
End If

LineNr = SVC.FindLine("����� ������� ������� � �������� ����", 0)
If LineNr > -1 Then
 If UBound(MTitles) <> 0 Then Redim Preserve MTitles(UBound(MTitles) + 1)
 MTitles(UBound(MTitles)) = "--- � �������� ����:"
 AddMoviesTitles (LineNr)
End If
TextBlock = SVC.GetBlockFrom("����� ������� ������� � ���� ������������")
If Len(TextBlock) > 0 Then
 If UBound(MTitles) <> 0 Then Redim Preserve MTitles(UBound(MTitles) + 1)
 MTitles(UBound(MTitles)) = "--- � ���� ������������:"
 AddReleasesTitles (TextBlock)
End If
TextBlock = SVC.GetBlockFrom("����� ������� ������� � ���� ������� DVD")
If Len(TextBlock) > 0 Then
 If UBound(MTitles) <> 0 Then Redim Preserve MTitles(UBound(MTitles) + 1)
 MTitles(UBound(MTitles)) = "--- � ���� ������� DVD:"
 AddReleasesTitles (TextBlock)
End If
End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line, MovieTitle, MovieAddress
 Dim StartPos, EndPos, tmp, i

Do
 LineNr = LineNr + 1
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "=""")
 EndPos = InStr(Line, "</OL>")
 If (StartPos > 0) And (EndPos = 0) Then
  StartPos = StartPos + 2
  tmp = InStr(Line, """>")
  If tmp > StartPos Then
   MovieAddress = Mid(Line, StartPos, tmp - StartPos)
   StartPos = tmp + 2
   tmp = InStr(Line, "</a>")
   If tmp > 1 Then
    MovieTitle = Mid(Line, StartPos, tmp - 1)
    MovieTitle = SVC.HTML2TEXT(MovieTitle)
    i = UBound(MTitles) + 1
    Redim Preserve MTitles(i): MTitles(i) = MovieTitle
    ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
   End If
  End If
 End If
Loop While EndPos <= 0
End Sub


Sub AddReleasesTitles(TextBlock)
 Dim MovieTitle, MovieAddress
 Dim StartPos, EndPos, EndTablePos, i

Do
 StartPos = InStr(TextBlock, "<td><b><a href=""")
 EndTablePos = InStr(TextBlock, "</table>")
 If StartPos > 0 Then
  TextBlock = Right(TextBlock, Len(TextBlock) - StartPos + 1)
  If StartPos < EndTablePos Then
   EndPos = InStr(TextBlock, "</td>")
   MovieTitle = Mid(TextBlock, 1, EndPos - 1)
   MovieTitle = SVC.HTML2TEXT(MovieTitle)
   MovieAddress = Mid(TextBlock, 17, InStr(TextBlock, """>") - 17)
   i = UBound(MTitles) + 1
   Redim Preserve MTitles(i): MTitles(i) = MovieTitle
   ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
   TextBlock = Right(TextBlock, Len(TextBlock) - EndPos + 1)
  End If
 End If
Loop Until (StartPos = 0) Or (StartPos > EndTablePos)
End Sub
                              

Sub AnalyzeMoviePage(Ind)
 Dim Line, Value, Value2
 Dim LineNr, BeginPos, EndPos

If SVC.FindLine("<TITLE>��������(R) - ����� �� DVD", 0) > -1 Then
 AnalyzeVideoPage(Ind)
 Exit Sub
ElseIf SVC.FindLine("<TITLE>��������(R) - ����� �� �����", 0) > -1 Then
 AnalyzeVideoPage(Ind)
 Exit Sub
End If

'Original Title & Year
LineNr = SVC.FindLine("<TITLE>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, "<TITLE>��������(R) - ")
 If BeginPos > 0 Then
  BeginPos = BeginPos + 21
  EndPos = InStr(BeginPos, Line, "&nbsp;(")
 End If
 If EndPos = 0 Then EndPos = Len(Line)
 If EndPos > BeginPos And BeginPos > 0 Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  Value = SVC.HTML2TEXT(Value)
  MTitle = SVC.UcaseFirst(Value)
 Else
  Exit Sub
 End If

 BeginPos = InStr(BeginPos, Line, "(")
 If BeginPos > 0 Then
  EndPos = Len(Line)
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
  End If
  BeginPos = InStr(Value, "(") + 1
  EndPos = InStr(BeginPos, Value, ")")
  If (BeginPos > 0) And (EndPos > 0) And (EndPos > BeginPos) Then
   MYear = Mid(Value, BeginPos, EndPos - BeginPos)
  End If
 End If

 'Director
 LineNr = SVC.FindLine("��������:", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "��������:") + 9
  EndPos = Len(Line)
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MDirector = SVC.HTML2TEXT(Value)
  End If
 End If

 'Actors
 LineNr = SVC.FindLine("�&nbsp;�����:", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "�&nbsp;�����:") + 13
  EndPos = Len(Line)
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MActors = SVC.HTML2TEXT(Value)
  End If
 End If

 'Category
 LineNr = SVC.FindLine("IdGenre=", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, """>") + 2
  EndPos = InStr(BeginPos, Line, "</a>")
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value,"/",", ")
   MGenre = SVC.HTML2TEXT(Value)
  End If
 End If

 'Country
 LineNr = SVC.FindLine("<em>", LineNr)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "<em>") + 4
  EndPos = InStr(BeginPos, Line, "&nbsp;")
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MCountry = SVC.HTML2TEXT(Value)
  End If
 End If

 'Year
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr + 1)
  BeginPos = 1 
  EndPos = InStr(BeginPos, Line, ";&nbsp;")
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = SVC.HTML2TEXT(Value)
   If MYear = "" Or Not IsNumeric(Left(MYear, 4)) Then
    MYear = Value
   End If
  End If
 End If

 'Description
 Value2 = "<p align=""JUSTIFY"">"
 LineNr = SVC.FindLine(Value2, 0)
 If LineNr = -1 Then
  Value2 = "<p align=justify>"
  LineNr = SVC.FindLine(Value2, 0)
 End If
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, Value2) + Len(Value2)
  EndPos = Len(Line)
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MDescription = SVC.HTML2TEXT(Value)
  End If
 End If

 'Picture
 LineNr = SVC.FindLine("<img src=""\img\films\", 0)
 If LineNr = -1 Then
  LineNr = SVC.FindLine("<img src=""/img/films/", 0)
 End If
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "src=""") + 6
  If BeginPos > 0 Then
   Line = Right(Line, Len(Line) - BeginPos + 1)
   EndPos = InStr(Line, """")
   BeginPos = 1
   If EndPos > BeginPos Then
    Value = Mid(Line, 1, EndPos - BeginPos)
    MPicURL = BaseAddress & Value
   End If
  End If
 End If

End If
End Sub

Public Sub AnalyzeVideoPage(Ind)
  Dim Line, Value, Value2
  Dim LineNr, BeginPos, EndPos 

'Title
LineNr = SVC.FindLine("<p><b><a href=""card_film.asp?IDFilm=", 0)
If LineNr = -1 Then LineNr = SVC.FindLine("<p><b><font color=""Navy"">", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 BeginPos = InStr(Line, """>") + 2 
 EndPos = InStr(Line, "</b>")
 If EndPos = 0 Then EndPos = Len(Line)
 If EndPos > BeginPos And BeginPos > 0 Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  Value = SVC.HTML2TEXT(Value)
  Value = SVC.UcaseFirst(Value)
 End If
 BeginPos = InStr(Line, "<br><small>/") + 12
 EndPos = InStr(Line, "/ &nbsp;")
 If (BeginPos > 0) And (EndPos > 0) Then
  If EndPos > BeginPos And BeginPos > 0 Then
   Value2 = Mid(Line, BeginPos, EndPos - BeginPos)
   Value2 = SVC.HTML2TEXT(Value2)
   Value2 = SVC.UcaseFirst(Value2)
   MTitle = Value & " (" & Value2 & ")"
  End If
  Line = Right(Line, Len(Line) - EndPos - 7)
 End If
   
 'Country
 BeginPos = InStr(1, Line, "<br><small>")
 If BeginPos = 0 Then BeginPos = 1
 EndPos = InStr(Line, " &nbsp;")
 If EndPos > BeginPos And BeginPos > 0 Then
  Value = Mid(Line, BeginPos, EndPos - BeginPos)
  Value = Replace(Value,"-",",")
  MCountry = SVC.HTML2TEXT(Value)
  Line = Right(Line, Len(Line) - EndPos - 6)
 End If

 'Year
 BeginPos = 1
 EndPos = InStr(Line, " &nbsp;")
 If EndPos > BeginPos And BeginPos > 0 Then
  MYear = Mid(Line, BeginPos, EndPos - BeginPos)
 End If

 'Category
 LineNr = SVC.FindLine("</small><br", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "</small>") + 8
  EndPos = Len(Line) + 1
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value,"/",", ")
   MGenre = SVC.HTML2TEXT(Value)
  End If
 End If

 'Director
 LineNr = SVC.FindLine("��������:", 0)
 If LineNr > -1 Then
   Line = SVC.PageArr(LineNr)
   BeginPos = InStr(Line, "��������:") + 9
   EndPos = Len(Line) + 1
   If EndPos > BeginPos And BeginPos > 0 Then
    Value = Mid(Line, BeginPos, EndPos - BeginPos)
    MDirector = SVC.HTML2TEXT(Value)
   End If
 End If

 'Actors
 LineNr = SVC.FindLine("<b>� �����:</b>", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "� �����:") + 8
  If BeginPos = 8 Then BeginPos = InStr(Line, "�&nbsp;�����:") + 13
  EndPos = Len(Line)
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos + 1)
   MActors = SVC.HTML2TEXT(Value)
  End If
 End If

 'Description
 LineNr = SVC.FindLine("<p align=""JUSTIFY"">", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(1, Line, "<p align=""JUSTIFY"">", vbTextCompare)
  EndPos = Len(Line)
  If EndPos > BeginPos And BeginPos > 0 Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MDescription = SVC.HTML2TEXT(Value)
  End If
 End If

 'Picture
 LineNr = SVC.FindLine("<img src=""\img\films\", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""/img/films/", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""\img\rel\", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""/img/rel/", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""\img\dvd_rel\", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""/img/dvd_rel/", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""\img\dvd\", 0)
 If LineNr = -1 Then LineNr = SVC.FindLine("<img src=""/img/dvd/", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "src=""") + 6
  EndPos = InStr(BeginPos, Line, """")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MPicURL = BaseAddress & Value
  End If
 End If
End If
End Sub

