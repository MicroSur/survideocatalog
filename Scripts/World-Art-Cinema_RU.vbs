'
'World-Art cinema html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2006-2008.
'
'NoCastFlag = False (����� ������� / Jeff Centauri - �������)
'NoCastFlag = True  (����� ������� / Jeff Centauri)

Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://www.world-art.ru/search.php?sector=cinema&name="

Dim BaseAddress
BaseAddress = "http://www.world-art.ru/"

Dim NoCastFlag
NoCastFlag = True

Sub AnalyzePage()
 Dim LineNr 
 Dim Line, Value
 Dim BeginPos, EndPos

LineNr = SVC.FindLine("<b>������ ", 0)
If LineNr > -1 Then 
 AddMoviesTitles (LineNr)
Else
  LineNr = SVC.FindLine("not_connect.htm", 0)
  If LineNr > -1 Then
   MTitles(0) = "WA Anime: ������ �� ��������."
   Exit Sub
  End If

 '��������
 LineNr = SVC.FindLine("<meta http-equiv='Refresh'", 0)
 If LineNr > -1 Then 
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "url=/cinema/") + 12
  EndPos = InStr(Line, "'>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   ReDim MData(0): MData(0) = SVC.MyVal(Value)
   Value = BaseAddress & Value
   SVC.ChangePage(Value)
   AnalyzeMoviePage 0  
   SVC.PutToSVC
  End If
 Else
  MTitles(0) = "WA Cinema: ������ �� �������."
 End If
End If

End Sub

Sub AddMoviesTitles(LineNr)
 Dim Line, Value
 Dim MovieTitle, MovieAddress, MovieID
 Dim StartPos, EndPos, i

 Line = SVC.PageArr(LineNr)
 EndPos = 1

 Do
  StartPos = InStr(EndPos, Line, "cinema/cinema.php?")
  If StartPos > 0 Then
   EndPos = InStr(StartPos, Line, ">")
   If EndPos > StartPos Then
    MovieAddress = Mid(Line, StartPos, EndPos - StartPos -1)
    If InStr(MovieAddress, """ class") > 0 Then MovieAddress = Left(MovieAddress, InStr(MovieAddress, """ class") - 1)
    StartPos = EndPos +1
    EndPos = InStr(StartPos, Line, "</a>")
    If EndPos > StartPos Then
     MovieTitle = Mid(Line, StartPos, EndPos - StartPos)
     MovieTitle = SVC.HTML2TEXT(MovieTitle)
     Redim Preserve MTitles(i): MTitles(i) = MovieTitle
     ReDim Preserve MTitlesURL(i): MTitlesURL(i) = BaseAddress & MovieAddress
     ReDim Preserve MData(i): MData(i) = SVC.MyVal(MTitlesURL(i))
     i = i + 1
    End If
   End If
  Else
   Exit Do
  End If
 Loop 

End Sub


Sub AnalyzeMoviePage(Ind)
 Dim Line, Value, Block
 Dim LineNr, BlockNr
 Dim BeginPos, EndPos
 Dim CRLF
 Dim NextURL

 CRLF = CHR(13) & CHR(10)

 '��������
 LineNr = SVC.FindLine("content=", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, "content='") + 9
  EndPos = InStr(BeginPos, Line, "'>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos) 
   MTitle = Replace(Value, " ()", "")
  End If
 End If

 '���
 LineNr = SVC.FindLine ("list.php?year=", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, "list.php?year=")
  EndPos = InStr(BeginPos, Line, ">")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MYear = SVC.MyVal(Value)
  End If
 End If

 '������
 LineNr = SVC.FindLine (">������������</b>:", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">������������</b>:") + 18
  EndPos = InStr(BeginPos, Line, "<br>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MCountry = SVC.HTML2TEXT(Value)
  End If
 End If

 '����
 LineNr = SVC.FindLine ("b>����</b>:", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, "b>����</b>:") + 11
  EndPos = InStr(BeginPos, Line, "<br>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   MGenre = SVC.HTML2TEXT(Value)
   MGenre = SVC.ConvStr(MGenre, 3)
  End If
 End If

 '�������
 LineNr = SVC.FindLine (">������� ����</b>:", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr)
  BeginPos = InStr(Line, ">������� ����</b>:") + 18
  EndPos = InStr(BeginPos, Line, "<br>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "&", "")
   Value = Replace(Value, "%", "")
   MRating = SVC.MyVal(Value)
  End If
 End If

 '����������
 LineNr = SVC.FindLine (">������� ����������:", 0)
 If LineNr > -1 Then
  Block = SVC.GetBlockFrom(">������� ����������:", "</p></td>")
  BeginPos = InStr(Block, ">������� ����������:") + 20
  EndPos = Len(Block)
  If EndPos > BeginPos Then
   Value = Mid(Block, BeginPos, EndPos - BeginPos + 1)
   MDescription = SVC.HTML2TEXT(Value)
  End If
 End If

'������� ��������� �����
 LineNr = SVC.FindLine (">� ����� �������", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">� ����� �������")
  EndPos = InStr(BeginPos, Line, "<br></td></tr></table>")          '�� �����
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = SVC.HTML2TEXT(Value)
   If Len(MDescription) = 0 Then
    MDescription = SVC.HTML2TEXT(Value)
   Else
    MDescription = MDescription & CRLF & CRLF & Value
   End If
  End If
 End If

'��������
Block = SVC.GetBlockFrom("<img src='img/")
If Len(Block)>0 Then
 EndPos = InStr(Block, "jpg")
 If EndPos > 1 Then
  Value = SVC.MyVal(Mid(Block, 1, EndPos))
 End If
End If
MPicURL = BaseAddress & "cinema/img/" & Value & "/" & MData(Ind) & "/1.jpg"


 '������
 LineNr = SVC.FindLine ("cinema_full_cast.php?id=", 0)
 If LineNr > -1 Then 
  '������ � ��������� �� ������ ��������
  NextURL = BaseAddress & "cinema/cinema_full_cast.php?id=" & MData(ind)
  SVC.ChangePage(NextURL) 
  
 Else

  '����� ��� ����
  '���������
  LineNr = SVC.FindLine (">�������</b>:", 0)
  If LineNr > -1 Then
   Line = SVC.PageArr(LineNr) 
   BeginPos = InStr(Line, ">�������</b>:") + 14
   EndPos = InStr(BeginPos, Line, "<br>")
   If EndPos > BeginPos Then
    Value = Mid(Line, BeginPos, EndPos - BeginPos)
    MDirector = SVC.HTML2TEXT(Value)
   End If
  End If

  '������
  LineNr = SVC.FindLine (">� �����</b>:", 0)
  If LineNr > -1 Then 
   Line = SVC.PageArr(LineNr) 
   BeginPos = InStr(Line, ">� �����</b>:") + 13
   EndPos = InStr(BeginPos, Line, "<br>")
   If EndPos > BeginPos Then
    Value = Mid(Line, BeginPos, EndPos - BeginPos)
    Value = SVC.HTML2TEXT(Value)
    MActors = Replace (Value, "� ������","")
   End If
  End If

 End If

 '������ ������ � ��������� (��������� ������)
 '��������
 LineNr = SVC.FindLine (">��������:<", 0)
 If LineNr > -1 Then
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">��������:<") + 10
  EndPos = InStr(BeginPos, Line, "</table>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "<tr>",",")
   Value = SVC.HTML2TEXT(Value)
   MDirector = Replace(Value, " ,",",")
  End If
 End If

 LineNr = SVC.FindLine (">���� ���������:", 0)
 If LineNr > -1 Then 
  Line = SVC.PageArr(LineNr) 
  BeginPos = InStr(Line, ">���� ���������:") + 16
  EndPos = InStr(BeginPos, Line, "</table>")
  If EndPos > BeginPos Then
   Value = Mid(Line, BeginPos, EndPos - BeginPos)
   Value = Replace(Value, "<tr>",",")
   If NoCastFlag Then
    Value = Replace(Value, "> &#151;","")
   End If
   Value = SVC.HTML2TEXT(Value)
   'Value = Replace(Value, " ����� � �����:,", "")
   MActors = Replace(Value, " ,",",")
  End If
 End If

End Sub
