'
'DVD Empire.com html pages parser. 
'VbScript adaptation by Sur for Sur Video Catalog. 2005-2008.
'
Option Explicit

Dim MTitle, MYear, MGenre, MDirector, MActors, MDescription, MCountry, MPicURL
DIM MRating, MLang, MSubt, MOther
Dim MTitles(), MTitlesURL(), MData()
Redim MTitles(0)
Dim url
url = "http://www.dvdempire.com/Exec/v5_search_item.asp?string="
Dim BaseAddress
BaseAddress = "http://www.dvdempire.com/Exec/v4_item.asp?item_id="

Sub AnalyzePage()
 Dim LineNr, i, j
 Dim Line, StartPos, EndPos
 Dim ItemID, MovieAddress, MovieTitle
 Dim Value

LineNr = SVC.FindLine("<title>DVD Empire - Item -", 0)
If LineNr > 0 Then
 LineNr = SVC.FindLine("<a href='/Exec/v4_item.asp?", 0)
 If LineNr > 0 Then
  Line = Trim(SVC.PageArr(LineNr))
  StartPos = InStr(Line, "item_id=") + 8
  If StartPos > 8 Then 
   EndPos = InStr(StartPos, Line, "&")
   If (EndPos > StartPos) Then
    ItemID = Mid(Line, StartPos, EndPos - StartPos)
    MovieAddress = BaseAddress & ItemID
    ReDim MTitlesURL(0): MTitlesURL(0) = MovieAddress
    ReDim MData(0): MData(0) = ItemID	
    AnalyzeMoviePage 0
    SVC.PutToSVC
    Exit Sub
   End If
  End If
 End If
Else
 LineNr = SVC.FindLine("> Matches", 0)
 If LineNr > 0 Then
  Line = SVC.PageArr(LineNr)
  j = SVC.MyVal(Line)
  If j > 0 Then
   i = 0
   StartPos = 1
   Do
    j = j - 1
    LineNr = SVC.FindLine("&amp;searchID=", LineNr)
    If LineNr > 0 Then
     Line = SVC.PageArr(LineNr)
     ItemId = ""
     If StartPos > 0 Then
      StartPos = InStr(StartPos, Line, "&amp;searchID=") - 30    
      If StartPos > 0 Then
       StartPos = InStr(StartPos, Line, "item_id=") + 8
       EndPos = InStr(StartPos, Line, "&")
       If EndPos > StartPos And StartPos > 8 Then
        ItemID = Mid(Line, StartPos, EndPos - StartPos)
       End If
       If ItemID <> "" Then
        MovieAddress = BaseAddress & ItemID
        StartPos = InStr(EndPos, Line, ">") + 1
        EndPos = InStr(StartPos, Line, "<")
        If EndPos > StartPos Then
         Value = Mid(Line, StartPos, EndPos - StartPos)
         MovieTitle = SVC.HTML2TEXT(Value)
         Redim Preserve MTitles(i): MTitles(i) = MovieTitle
         ReDim Preserve MTitlesURL(i): MTitlesURL(i) = MovieAddress
         ReDim Preserve MData(i): MData(i) = ItemID	
         i = i + 1
        End If
       End If
      End If
     End If
    End If 
   Loop While j > 0 'LineNr <> -1
  Else
   MTitles(0) = "DVDEmpire: no matches found."
  End If
 End If
End If
  
End Sub

Sub AnalyzeMoviePage(Ind)
 Dim Line, Value
 Dim LineNr, StartPos, EndPos
 Dim LineNr2, i, Tmp, Rt
'Title
LineNr = SVC.FindLine("<title>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "- Item - ")
 If StartPos > 0 Then
  StartPos = StartPos + 9
  EndPos = InStr(StartPos, Line, " /")
  If EndPos > StartPos Then
   Value = Mid(Line, StartPos, EndPos - StartPos)
   MTitle = SVC.HTML2TEXT(Value)
  End If
 End If
End If

'Description
LineNr = SVC.FindLine("<b>Synopsis</b>", 0)
If LineNr > -1 Then
 LineNr = SVC.FindLine("<td", LineNr)
 If LineNr > -1 Then 
  LineNr = LineNr + 1
  LineNr2 = SVC.FindLine("</table>", LineNr)
  If LineNr2 > 0 Then
   Value = ""
   For i = LineNr To LineNr2
    Value = Value & SVC.PageArr(i)
   Next
   Value = Replace(Value, ">i<", "> <")
   MDescription = SVC.HTML2TEXT(Value)
  End If
 End If
End If

'Actors
LineNr = SVC.FindLine("<b>Actors:</b>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "Actors:") + 7
' EndPos = InStr(StartPos, Line, "<b>Directors:</b>")
 EndPos = InStr(StartPos, Line, "'left'><b>") + 10
 If EndPos > 0 Then Line = Mid(Line, StartPos, EndPos - StartPos)
 If InStr(Line, "&nbsp;") > 0 Then
  Value = ""
  EndPos = 1
  Do
   StartPos = InStr(EndPos, Line, "&#149;") + 6
   EndPos = InStr(StartPos, Line, "<br")
   If EndPos > StartPos And StartPos > 6 Then
    Tmp = Mid(Line, StartPos, EndPos - StartPos)
    Tmp = SVC.HTML2TEXT(Tmp)
    Value = Value & ", " & Tmp
   Else
    Exit Do
   End If
  Loop
 End If
 If Len(Value) > 2 Then MActors = Right(Value, Len(Value) - 2)
End If

'Director
LineNr = SVC.FindLine("<b>Directors:</b>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "Directors:") + 10
 'EndPos = InStr(StartPos, Line, "<b>Producers:</b>")
 EndPos = InStr(StartPos, Line, "'left'><b>") + 10
 If EndPos > 0 Then Line = Mid(Line, StartPos, EndPos - StartPos)
 If InStr(Line, "&nbsp;") > 0 Then
  Value = ""
  EndPos = 1
  Do
   StartPos = InStr(EndPos, Line, "&#149;") + 6
   EndPos = InStr(StartPos, Line, "<br")
   If EndPos > StartPos And StartPos > 6 Then
    Tmp = Mid(Line, StartPos, EndPos - StartPos)
    Tmp = SVC.HTML2TEXT(Tmp)
    Value = Value & ", " & Tmp
   Else
    Exit Do
   End If
  Loop
 End If
 If Len(Value) > 2 Then MDirector = Right(Value, Len(Value) - 2)
End If

'Studio
LineNr = SVC.FindLine("<b>Studio:</b>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "Studio:") + 7 
 EndPos = InStr(StartPos, Line, "<br")
 If EndPos > StartPos Then
  Value = Mid(Line, StartPos, EndPos - StartPos)
  MCountry = SVC.HTML2TEXT(Value)
 End If
End If

'Year
LineNr = SVC.FindLine("<b>Production Year:</b>", LineNr)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "<b>Production Year:</b>") + 23 
 EndPos = InStr(StartPos, Line, "<br")
 If EndPos > StartPos Then
  Value = Mid(Line, StartPos, EndPos - StartPos)
  MYear = SVC.HTML2TEXT(Value)
 End If
End If

'Subt
'LineNr = SVC.FindLine("<b>Subtitles:</b>", 0)
'If LineNr > -1 Then
' Line = SVC.PageArr(LineNr)
' StartPos = InStr(Line, "<b>Subtitles:</b>") + 17
' EndPos = Len(Line)
' If EndPos > StartPos Then
'  Value = Mid(Line, StartPos, EndPos - StartPos) 
'  MSubt = SVC.HTML2TEXT(Value)
' End If
'End If

'Genre
LineNr = SVC.FindLine("<b>Genre</b>:", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "<b>Genre</b>:") + 13
 EndPos = Len(Line)
 If EndPos > StartPos Then
  Value = Mid(Line, StartPos, EndPos - StartPos) 
  MGenre = SVC.HTML2TEXT(Value)
 End If
End If

'Rating
LineNr = SVC.FindLine("<b>Overall Rating:</b>", 0)
If LineNr > -1 Then
 Line = SVC.PageArr(LineNr)
 StartPos = InStr(Line, "&nbsp;<b>") + 9
 EndPos = InStr(StartPos, Line, "</b")
 If EndPos > StartPos Then
  Rt = Mid(Line, StartPos, EndPos - StartPos)
  Rt = Replace(Rt, ".",",")
  If IsNumeric(Rt) Then 
   MRating = 2 * Rt  '�� 10
  Else
   MRating = Rt & "/5"
  End If
 End If
End If
  
'Picture
MPicURL = "http://images.dvdempire.com/gen/movies/" & MData(Ind) & "h.jpg"
End Sub
                                    